User story :
En tant qu’acheteur je souhaite consulter des produits afin de faire l’achat que je veux
En tant qu’acheteur je souhaite enregistrer un achat afin de saisir les infos de livraison
En tant qu’acheteur je souhaite enregistrer un achat afin d’enregistrer un règlement
En tant qu’acheteur je souhaite enregistrer un achat afin de constituer un panier
En tant qu’acheteur je souhaite m’authentifier afin d’enregistrer un achat
En tant que livreur je veux pouvoir préparer une livraison
En tant que technicien je veux consulter les remarques

